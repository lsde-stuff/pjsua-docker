FROM alpine
RUN apk add --update --no-cache pjsua
WORKDIR /tmp
USER nobody
COPY thank-you-one-moment.wav .
ENTRYPOINT ["/usr/bin/pjsua"]
CMD ["--help"]
